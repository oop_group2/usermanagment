/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waratchaya.usermanagment.newpackage;

/**
 *
 * @author Melon
 */
public class TestUserService {

    public static void main(String[] args) {
        UserService.addUser("mind", "12345");
        System.out.println(UserService.getUsers());
        UserService.addUser(new User("user1", "1234"));
        System.out.println(UserService.getUsers());

        User user = UserService.getUser(3);
        System.out.println(user);
        user.setPassword("5555");
        UserService.updataUser(3, user);
        System.out.println(UserService.getUsers());
        
        System.out.println(UserService.getUsers());
        System.out.println(UserService.login("mind", "12345"));
    }
}
